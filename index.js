var AWS = require("aws-sdk");

AWS.config.update({
  region: "us-east-1"
});

var docClient = new AWS.DynamoDB.DocumentClient();

var table = "books";

// need for cross-site POST to work
const corsHeaders = {
    'Access-Control-Allow-Methods': '*',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'content-type'
};

async function newBook(book) {
    book.isbn = parseInt(book.isbn);
    book.pages = parseInt(book.pages);
    let params = {
        TableName: table,
        Item: book
    };
    
    const awsRequest = docClient.put(params);
    return awsRequest.promise()
    .then(data => {
        console.log("Yay!");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    },err => {
        console.log("Uh-oh!");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}

async function getBooks(){
    let params = {
        TableName: table
    };
    
    const awsRequest = docClient.scan(params);
    return awsRequest.promise()
    .then(data => {
        console.log("Got all books!");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    }, err => {
        console.log("Failed to get all books!");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}

async function deleteBook(isbn){
    let params = {
        TableName: table,
        Key: {
            'isbn': parseInt(isbn)
        }
    }
    
    const awsRequest = docClient.delete(params);
    
    console.log("Book to delete isbn: " + isbn);
    
    return awsRequest.promise()
    .then(data => {
        console.log("Book successfully deleted!");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    }, err => {
        console.log("Failed to delete the book!");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}

exports.handler = async (event) => {
    // sample event properties:
    // event.path: '/default/library/books'
    // event.httpMethod: 'POST'
    // event.body: json encoded object

    console.log( event.path );
    console.log( event.httpMethod );
    let body = JSON.parse(event.body);
    console.log( body );
    
    if ( event.httpMethod == 'POST' && event.path == '/default/library/books') {
        return newBook(body);
    }
    
    if ( event.httpMethod == 'GET' && event.path == '/default/library/books') {
       return getBooks();
    }
    
    if ( event.httpMethod == 'DELETE' && event.path == '/default/library/books/') {
        let isbn = event.path.substring(23);
        
        return deleteBook(isbn);
    }
    
    const response = {
        statusCode: 200,
        headers: corsHeaders,
        body: JSON.stringify('Unrecognized request'),
    };
    return response;
};
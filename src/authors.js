import React, { Component } from 'react';

import axios from 'axios';

// UPDATE WITH YOUR API ENDPOINT
const baseURL ="https://iz7lz0mf53.execute-api.us-east-1.amazonaws.com/default/library";

class Authors extends Component{
    constructor(props){
        super(props);
        this.state = {
            authors: [],
        }
    }
    
    addAuthor(ev) {
        const inputAID = ev.target.querySelector('[id="aid"]');
        const aid = inputAID.value.trim();
    
        const inputFName = ev.target.querySelector('[id="fname"]');
        const fname = inputFName.value.trim();
    
        const inputLName = ev.target.querySelector('[id="lname"]');
        const lname = inputLName.value.trim();
    
        console.log( "aid: " + aid );
        console.log( "First Name: " + fname );
        console.log( "Last Name: " + lname );
        
        let newauthor = {
          aid,
          fname,
          lname
        };
        
        axios.post( baseURL + "/books", newauthor)
        .then(res => {
          let newAuthors = this.state.authors;
          // console.log(newBooks);
          newAuthors.push(newauthor);
          // console.log(newBooks);
          this.setState({
            authors: newAuthors
          })
        });
        
        ev.preventDefault();
    }
    
    render() {


        return(
        <div>
          <div className="py-5 text-center">
            <h2>Authors</h2>
          </div>
    
          <div className="row">
            <div className="col-md-12 order-md-1">
              <form onSubmit={this.addAuthor.bind(this)} className="needs-validation" noValidate>
                <div className="row">
                  <div className="col-md-2 mb-3">
                    <label htmlFor="aid">AID</label>
                    <input type="number" className="form-control" id="aid" defaultValue="1" required />
                    <div className="invalid-feedback">
                        An AID is required.
                    </div>
                  </div>
    
                  <div className="col-md-5 mb-3">
                    <label htmlFor="fname">First Name</label>
                    <input type="text" className="form-control" id="fname" defaultValue="" required />
                    <div className="invalid-feedback">
                        A first name is required.
                    </div>
                  </div>
    
                  <div className="col-md-5 mb-3">
                    <label htmlFor="lname">Last Name</label>
                    <input type="text" className="form-control" id="lname" defaultValue="" required />
                    <div className="invalid-feedback">
                        A last name is required.
                    </div>
                  </div>
    
                </div>
                <button className="btn btn-primary btn-lg btn-block" type="submit">Add author</button>
              </form>
            </div>
          </div>
          
          <table className="table">
            <thead>
              <tr>
                <th scope="col">AID</th>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
              </tr>
            </thead>
            <tbody>
              {this.state.authors && this.state.authors.map( author =>
                <tr key={author.aid}>
                  <td>{author.aid}</td>
                  <td>{author.fname}</td>
                  <td>{author.lname}</td>
                </tr>
              )}
            </tbody>
          </table>
    
          <footer className="my-5 pt-5 text-muted text-center text-small">
            <p className="mb-1">&copy; 2020 CPSC 2650</p>
          </footer>
        </div>
        );
  }
}

export default Authors;
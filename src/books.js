import React, { Component } from 'react';

import axios from 'axios';

// UPDATE WITH YOUR API ENDPOINT
const baseURL ="https://iz7lz0mf53.execute-api.us-east-1.amazonaws.com/default/library";

class Books extends Component {
 
 constructor(props){
   super(props);
   this.state = {
     books: [],
   }
 }
 
 // Loads all books from DB and puts into state
 componentDidMount(){
   axios.get( baseURL + "/books")
   .then(res =>{
    // console.log(this.state);
    // console.log(res.data.Items);
     this.setState({
       books: res.data.Items
     });
   })
   .catch(error => {
     console.log(error);
   })
 }
 
  addBook(ev) {
    const inputISBN = ev.target.querySelector('[id="isbn"]');
    const isbn = inputISBN.value.trim();

    const inputTitle = ev.target.querySelector('[id="title"]');
    const title = inputTitle.value.trim();

    const inputPages = ev.target.querySelector('[id="pages"]');
    const pages = inputPages.value.trim();

    console.log( "ISBN: " + isbn );
    console.log( "Title: " + title );
    console.log( "Pages: " + pages );
    
    let newbook = {
      isbn,
      title,
      pages
    };
    
    axios.post( baseURL + "/books", newbook)
    .then(res => {
      let newBooks = this.state.books;
      // console.log(newBooks);
      newBooks.push(newbook);
      // console.log(newBooks);
      this.setState({
        books: newBooks
      })
    });
    
    ev.preventDefault();
  }
  
  deleteBook(isbn, ev){
    // console.log(isbn);
    axios.delete( baseURL + "/books/" + isbn)
    .then(res => {
      console.log("Book was deleted");
    })
    .catch(err => console.log(err));
    
    ev.preventDefault();
  }
    
  render() {


    return(
    <div>
      <div className="py-5 text-center">
        <h2>Books</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addBook.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-2 mb-3">
                <label htmlFor="isbn">ISBN</label>
                <input type="number" className="form-control" id="isbn" defaultValue="1" required />
                <div className="invalid-feedback">
                    An ISBN is required.
                </div>
              </div>

              <div className="col-md-8 mb-3">
                <label htmlFor="title">Title</label>
                <input type="text" className="form-control" id="title" defaultValue="" required />
                <div className="invalid-feedback">
                    A book title is required.
                </div>
              </div>

              <div className="col-md-2 mb-3">
                <label htmlFor="pages">Pages</label>
                <input type="number" className="form-control" id="pages" defaultValue="100" required />
                <div className="invalid-feedback">
                    The number of pages is required.
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add book</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ISBN</th>
            <th scope="col">Title</th>
            <th scope="col">Pages</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>
          {this.state.books && this.state.books.map( book =>
            <tr key={book.isbn}>
              <td>{book.isbn}</td>
              <td>{book.title}</td>
              <td>{book.pages}</td>
              <td><button className="btn btn-danger" type="button" onClick={this.deleteBook.bind(this, book.isbn)}>Delete</button></td>
            </tr>
          )}
        </tbody>
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Books;
